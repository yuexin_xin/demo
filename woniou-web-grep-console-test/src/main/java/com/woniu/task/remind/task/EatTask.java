package com.woniu.task.remind.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @author 蜗牛
 * @description 每天进步一点点
 * @date 2023/03/18 13:48
 */
@Slf4j
@Component
public class EatTask  {

    @Scheduled(cron = "0/5 * * * * ?")
    public void execute(){

        log.info("[吃饭提醒]主人,吃点东西吧"+ LocalDateTime.now());

    }
}
