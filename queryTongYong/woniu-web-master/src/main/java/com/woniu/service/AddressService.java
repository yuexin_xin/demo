package com.woniu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.woniu.address.Address;

/**
 * @className: AddressService
 * @author: woniu
 * @date: 2023/6/25
 **/
public interface AddressService extends IService<Address> {

}
