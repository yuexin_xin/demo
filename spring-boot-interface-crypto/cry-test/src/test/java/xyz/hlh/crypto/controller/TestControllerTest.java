package xyz.hlh.crypto.controller;

import cn.hutool.json.JSONUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import xyz.hlh.crypto.common.entity.Result;
import xyz.hlh.crypto.util.AESUtil;

/**
 * @author wn
 * @description:
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class TestControllerTest {

    @Test
    public void test1() {

        System.out.println(AESUtil.getAes());

        String data = JSONUtil.toJsonStr(new Result<String>());
        System.out.println(AESUtil.encryptHex(data));
        System.out.println(AESUtil.decrypt(AESUtil.encryptHex(data)));

    }
}