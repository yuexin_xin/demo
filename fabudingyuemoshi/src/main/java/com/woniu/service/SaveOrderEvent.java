package com.woniu.service;


import org.springframework.context.ApplicationEvent;

public class SaveOrderEvent extends ApplicationEvent {

 public SaveOrderEvent(SaveOrderEventMessage source) {
  super(source);
 }
}