package com.woniu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class WoNiuApplication {

    public static void main(String[] args) {
        SpringApplication.run(WoNiuApplication.class, args);
    }



}
