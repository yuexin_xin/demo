package com.woniu.service;

public interface IdempotentStorage {

    void save(String idempotentId);

    boolean delete(String idempotentId);
}